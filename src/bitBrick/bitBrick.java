package bitBrick;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import jssc.SerialPort;


public class bitBrick {
	String[] sensor_lookup = {"null","Brightness","Distance","Button","Potentiometer","MIC","6","7","8","9","10","UserInput", "12","13","14","15","16","17","DC motor","Servo motor","RGB LED"};
	String ports ="";
	byte[] outputData = {(byte)255, (byte)255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, (byte)254, (byte)254};
	byte[] readData = new byte[17];

	SerialPort serialPort;
	int port1value;
	int port2value;
	int port3value;
	int port4value;
	
	//비브브릭연결
	 public void connect(String portName) throws Exception {
		  serialPort = new SerialPort(portName);
		  serialPort.openPort();//Open serial port
		  serialPort.setParams(SerialPort.BAUDRATE_38400,
		                   SerialPort.DATABITS_8,
		                   SerialPort.STOPBITS_1,
		                   SerialPort.PARITY_NONE);
		new ReadThread(serialPort).start();
		new WriteThread(serialPort).start();
	 }

	public void stop() {
		outputData[3] = 0;
		outputData[5] = 0;
		outputData[7] = 0;
		outputData[9] = 0;
		outputData[11] = 0;
		outputData[13] = 0;
		outputData[15] = 0;
		outputData[17] = 0;
	}
	
	public void set_led_color(int color) {
		outputData[13] = 0;
		outputData[15] = 0;
		outputData[17] = 0;
	}  
	
	public void set_motor_all_stop() {
		outputData[5] = 0;
		outputData[7] = 0;
		outputData[9] = 0;
		outputData[11] = 0;
	}
	
	public void set_led_rgb(int r, int g, int b ) {
		int red = r;
		int green = g;
		int blue =b;
		if(red<0) {
			red=0;
		}else if(red>255) {
			red = 255;
		}
		if(green<0) {
			green = 0;
		}else if(green >255) {
			green = 255;
		}
		if(blue <0) {
			blue = 0;
		}else if(blue >255) {
			blue = 255;
		}
		outputData[13] = (byte) red;
		outputData[15] = (byte) green;
		outputData[17] = (byte) blue;
	}
	
	
	public void set_led_off() {
		outputData[13] = 0;
		outputData[15] = 0;
		outputData[17] = 0;
	}
	

	
	public void set_dc_motor_velocity(String port, int velocity) {
		int vel = velocity;
		if (vel >= -100 && vel <= 100) {
			vel = vel+ 128;
		}
		else if( vel < -100) {
			vel = 28;
		}
		else if (vel >100) {
			vel = 228;
		}
		
		if(port.equals("A") || port.equals("a")){
			System.out.print(vel);
			outputData[5] = (byte)vel;
		}else if(port.equals("B") || port.equals("b")) {
			outputData[7] = (byte)vel;

		}else if(port.equals("C") || port.equals("c")) {
			outputData[9] = (byte)vel;

		}else if(port.equals("D") || port.equals("d")) {
			outputData[11] = (byte)vel;
		}else{
			
		}
	}
	
	public void set_dc_motor_dir_speed(String port, String dir, int speed) {
		int  spd = speed;
		if(spd <0) {
			spd = 0;
		}
		else if (spd >100) {
			spd =100;
		}
		
		if (dir.equals("CW") || dir.equals("cw")) {
			spd = 128+spd;
		}else if(dir.equals("CCW") || dir.equals("ccw")) {
			spd = 128-spd;
		}
	
		if(port.equals("A") || port.equals("a")){
			outputData[5] = (byte)spd;
		}else if(port.equals("B") || port.equals("b")) {
			outputData[7] = (byte)spd;
		}else if(port.equals("C") || port.equals("c")) {
			outputData[9] = (byte)spd;
		}else if(port.equals("D") || port.equals("d")) {
			outputData[11] = (byte)spd;
		}else{
			            
		}
	}
	public void set_buzzer_num(int buzzer) {
		int value = buzzer;
		if (value <0) {
			value =0;
		}else if (value >96) {
			value = 96;
		}
		System.out.println(outputData);
		outputData[3] = (byte)value;
	}
	
	public void set_servo_motor(String port, int angle) {
		int ang = 0;
	      if(angle<=0){
	          ang = 181;
	        }
	        else if(angle>=180){
	          ang =1;
	        }else{
	          ang = (180-angle)+1;
	        }
      	        
	        if(port.equals("A")|| port.equals("a")){
	          outputData[5] =  (byte) ang;
	        }else if(port.equals("B")|| port.equals("b")){
	        	outputData[7] = (byte) ang;

	        }else if(port.equals("C")|| port.equals("c")){
	        	outputData[9] = (byte) ang;

	        }else if(port.equals("D")|| port.equals("d")){
	        	outputData[11] = (byte) ang;

	        }
	}
	
	public int get_sensor_value(int port) {
		if(port == 1) {
			return port1value = (int)((readData[2] & 0x03) << 8) + readData[3]; 
		}else if(port ==2) {
			return port2value = (int)((readData[4] & 0x03) << 8) + readData[5]; 
		}else if(port == 3) {
			return port3value = (int)((readData[6] & 0x03) << 8) + readData[7]; 
		}else if(port == 4) {
			return port4value = (int)((readData[8] & 0x03) << 8) + readData[9];
		}else {
			return 0;
		}
	}
	
	public Boolean get_button(int port) {
		int sen = (readData[port*2] & 0xFC ) >> 2;
		int val = (int)(((readData[port*2] & 0x03) << 8) + readData[port*2+1]);
		if(sen == 3 ) {
			if(val >1020) {
				return false;
			}else if(val < 2) {
				return true;
			}			
		}else{
			return false;
		}
		return false;
	}
	
	public String get_module_check(String port) {
		if (port.equals("1")) {
            return sensor_lookup[(readData[2] & 0xFC ) >> 2]; 
		}
		else if(port.equals("2")) {
            return sensor_lookup[ (readData[4] & 0xFC ) >> 2]; 
		}
		else if(port.equals("3")) {
            return sensor_lookup[ (readData[6] & 0xFC ) >> 2]; 
		}else if(port.equals("4")) {
            return sensor_lookup[ (readData[8] & 0xFC ) >> 2];
		}else if(port.equals("A") || port.equals("a")) {
            return sensor_lookup[readData[10]];
		}else if(port.equals("B") || port.equals("b")) {
            return sensor_lookup[readData[11]];
		}else if(port.equals("C") || port.equals("c")) {
            return sensor_lookup[readData[12] ];
		}else if(port.equals("D") || port.equals("d")) {
            return sensor_lookup[ readData[13] ];
		}else if(port.equals("LED")) {
            return sensor_lookup[ readData[14] ];
		}
       	else {
//            return '1:'+ self.sensor_lookup[ ( self.input_data[2] & 0xFC ) >> 2 ] , \n
//                   '2:'+ self.sensor_lookup[ ( self.input_data[4] & 0xFC ) >> 2 ] , \
//                   '3:'+ self.sensor_lookup[ ( self.input_data[6] & 0xFC ) >> 2 ] , \
//                   '4:'+ self.sensor_lookup[ ( self.input_data[8] & 0xFC ) >> 2 ] , \
//                   'A:'+ self.sensor_lookup[ self.input_data[10] ] , \
//                   'B:'+ self.sensor_lookup[ self.input_data[11] ] , \
//                   'C:'+ self.sensor_lookup[ self.input_data[12] ] , \
//                   'D:'+ self.sensor_lookup[ self.input_data[13] ] , \
//                   'LED:'+ self.sensor_lookup[ self.input_data[14] ]
       	return null;
       	}
	
	}
	class ReadThread extends Thread{
		SerialPort serial;
		ReadThread(SerialPort serial){
			this.serial = serial;
		}

		public void run() {
			try {
				while (true) {
					byte[] read = serial.readBytes();
					if(read != null && read.length >= 17) {
						if((read[0] & 0xFF) == 255 &&(read[1] & 0xFF) == 255 &&(read[15]& 0xFF) == 254 &&(read[16]& 0xFF) == 254) {
							readData = read;
						}
						
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class WriteThread extends Thread{
		SerialPort serial;
		WriteThread(SerialPort serial){
			this.serial = serial;
		}
		public void run() {
			try {
				
				int c = 0;
				System.out.println("\nKeyborad Input Read!!!!");
				while (true) {
					serial.writeBytes(outputData);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}



}





